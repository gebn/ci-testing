package main

import (
    "testing"
)

func TestAdder(t *testing.T) {
    table := []struct {
        a, b, want int
    } {
        {1, 2, 3},
        {4, 5, 9},
    }
    for _, row := range table {
        got := Add(row.a, row.b)
        if got != row.want {
            t.Errorf("Add(%v, %v) == %v, wanted %v", row.a, row.b, got, row.want)
        }
    }
}
